package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Solas.SolasRequest;
import SublimeGracia.dto.Solas.SolasResponse;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface ISolasService {
    @Cacheable("solas")
    List<SolasResponse> findAll();

    @Cacheable("solas")
    SolasResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "solas", allEntries = true)})
    SolasResponse save(SolasRequest solas);

    @Caching(evict = {@CacheEvict(value = "solas", allEntries = true)})
    SolasResponse update(Long id, SolasRequest solas);

    @Caching(evict = {@CacheEvict(value = "solas", allEntries = true)})
    void delete(Long id);
}
