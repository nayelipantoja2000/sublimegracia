package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Creencia.CreenciaRequest;
import SublimeGracia.dto.Creencia.CreenciaResponse;
import SublimeGracia.entity.Creencia;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface ICreenciaService {
    @Cacheable("creencia")
    List<CreenciaResponse> findAll();

    @Cacheable("creencia")
    CreenciaResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "creencia", allEntries = true)})
    CreenciaResponse save(CreenciaRequest creencia);

    @Caching(evict = {@CacheEvict(value = "creencia", allEntries = true)})
    CreenciaResponse update(Long id, CreenciaRequest creencia);

    @Caching(evict = {@CacheEvict(value = "creencia", allEntries = true)})
    void delete(Long id);

    @Caching(evict = {@CacheEvict(value = "creencia", allEntries = true)})
    List<Creencia> findByConfesionId(Long creenciaId);
}
