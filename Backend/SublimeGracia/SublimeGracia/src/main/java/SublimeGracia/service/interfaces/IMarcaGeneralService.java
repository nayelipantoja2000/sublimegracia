package SublimeGracia.service.interfaces;

import SublimeGracia.dto.MarcasGeneral.MarcaGeneralRequest;
import SublimeGracia.dto.MarcasGeneral.MarcaGeneralResponse;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface IMarcaGeneralService{
    @Cacheable("marcasGeneral")
    List<MarcaGeneralResponse> findAll();

    @Cacheable("marcasGeneral")
    MarcaGeneralResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "marcasGeneral", allEntries = true)})
    MarcaGeneralResponse save(MarcaGeneralRequest marcaGeneral);

    @Caching(evict = {@CacheEvict(value = "marcasGeneral", allEntries = true)})
    MarcaGeneralResponse update(Long id, MarcaGeneralRequest marcaGeneral);

    @Caching(evict = {@CacheEvict(value = "marcasGeneral", allEntries = true)})
    void delete(Long id);
}
