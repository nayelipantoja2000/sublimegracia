package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Pastor.PastorRequest;
import SublimeGracia.dto.Pastor.PastorResponse;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface IPastorService {
    @Cacheable("pastor")
    List<PastorResponse> findAll();

    @Cacheable("pastor")
    PastorResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "pastor", allEntries = true)})
    PastorResponse save(PastorRequest pastor);

    @Caching(evict = {@CacheEvict(value = "pastor", allEntries = true)})
    PastorResponse update(Long id, PastorRequest pastor);

    @Caching(evict = {@CacheEvict(value = "pastor", allEntries = true)})
    void delete(Long id);
}
