package SublimeGracia.service.interfaces;

import SublimeGracia.entity.Usuario;

public interface IMailService {
    void send(Usuario usuario);
}
