package SublimeGracia.service;

import SublimeGracia.service.interfaces.ITokenBlackListService;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class TokenBlackListService implements ITokenBlackListService {

    private static final Map<String, String> tokenBlacklist = new ConcurrentHashMap<>();

    public void add(String token) {
        tokenBlacklist.put(token, token);
    }

    public boolean contains(String token) {
        return tokenBlacklist.containsKey(token);
    }


}
