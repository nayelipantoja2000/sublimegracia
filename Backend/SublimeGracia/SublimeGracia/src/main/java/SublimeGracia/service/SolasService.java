package SublimeGracia.service;

import SublimeGracia.dto.Solas.SolasRequest;
import SublimeGracia.dto.Solas.SolasResponse;
import SublimeGracia.entity.Solas;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.ISolasRepository;
import SublimeGracia.service.interfaces.ISolasService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SolasService implements ISolasService {

    private final ISolasRepository solasRepository;

    @Override
    @Cacheable("solas")
    public List<SolasResponse> findAll() {
        return solasRepository.findAll().stream()
                .map(solas -> new SolasResponse(solas)).toList();
    }

    @Override
    @Cacheable("solas")
    public SolasResponse findById(Long id) {
        return solasRepository.findById(id)
                .map(solas -> new SolasResponse(solas))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "solas", allEntries = true)})
    public SolasResponse save(SolasRequest solas) {

        Solas solasToSave = new Solas();
        solasToSave.setDescripcion(solas.getDescripcion());
        solasToSave.setTitulo((solas.getTitulo()));
        return new SolasResponse(solasRepository.save(solasToSave));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "solas", allEntries = true)})
    public SolasResponse update(Long id, SolasRequest solas) {

        Solas solasToUpdate = solasRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (solas.getDescripcion() != null) {
            solasToUpdate.setDescripcion(solas.getDescripcion());
        }
        if (solas.getTitulo() != null) {
            solasToUpdate.setTitulo(solas.getTitulo());
        }

        return new SolasResponse(solasRepository.save(solasToUpdate));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "solas", allEntries = true)})
    public void delete(Long id) {
        Solas solasToDelete = solasRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("La sola con el ID %d no fue encontrado", id)));
        solasRepository.delete(solasToDelete);
    }
}
