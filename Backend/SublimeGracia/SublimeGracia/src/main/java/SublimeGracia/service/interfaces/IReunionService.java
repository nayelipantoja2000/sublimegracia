package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Reunion.ReunionRequest;
import SublimeGracia.dto.Reunion.ReunionResponse;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface IReunionService {
    @Cacheable("reunion")
    List<ReunionResponse> findAll();

    @Cacheable("reunion")
    ReunionResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "reunion", allEntries = true)})
    ReunionResponse save(ReunionRequest reunion);

    @Caching(evict = {@CacheEvict(value = "reunion", allEntries = true)})
    ReunionResponse update(Long id, ReunionRequest reunion);

    @Caching(evict = {@CacheEvict(value = "reunion", allEntries = true)})
    void delete(Long id);
}
