package SublimeGracia.service;

import SublimeGracia.service.interfaces.IMarcaGeneralService;
import SublimeGracia.service.interfaces.IMarcaService;
import SublimeGracia.dto.MarcasGeneral.MarcaGeneralRequest;
import SublimeGracia.dto.MarcasGeneral.MarcaGeneralResponse;
import SublimeGracia.entity.Marca;
import SublimeGracia.entity.MarcasGeneral;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.IMarcaGeneralRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MarcasGeneralService implements IMarcaGeneralService {

    private final IMarcaGeneralRepository marcaGeneralRepository;
    private final IMarcaService marcaService;

    @Override
    @Cacheable("marcasGeneral")
    public List<MarcaGeneralResponse> findAll() {
        return marcaGeneralRepository.findAll().stream()
                .map(marcas -> {
                    List<Marca> marca = marcaService.findByMarcaGeneralId(marcas.getId());
                    return new MarcaGeneralResponse(marcas, marca);
                })
                .toList();
    }

    @Override
    @Cacheable("marcasGeneral")
    public MarcaGeneralResponse findById(Long id) {
        List<Marca> marca = marcaService.findByMarcaGeneralId(id);
        return marcaGeneralRepository.findById(id)
                .map(marcas -> new MarcaGeneralResponse(marcas, marca))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "marcasGeneral", allEntries = true)})
    public MarcaGeneralResponse save(MarcaGeneralRequest marcaGeneral) {

        MarcasGeneral marcasToSave = new MarcasGeneral();
        marcasToSave.setEncabezado(marcaGeneral.getEncabezado());
        marcasToSave.setUrlMarcas(marcaGeneral.getUrlMarcas());
        marcasToSave.setDescripcion(marcaGeneral.getDescripcion());

        List<Marca> marcas = Collections.emptyList();
        return new MarcaGeneralResponse(marcaGeneralRepository.save(marcasToSave), marcas);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "marcasGeneral", allEntries = true)})
    public MarcaGeneralResponse update(Long id, MarcaGeneralRequest marcaGeneral) {

        MarcasGeneral marcaGeneralToUpdate = marcaGeneralRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (marcaGeneral.getEncabezado() != null) {
            marcaGeneralToUpdate.setEncabezado(marcaGeneral.getEncabezado());
        }
        if (marcaGeneral.getUrlMarcas() != null) {
            marcaGeneralToUpdate.setUrlMarcas(marcaGeneral.getUrlMarcas());
        }
        if (marcaGeneral.getDescripcion() != null) {
            marcaGeneralToUpdate.setDescripcion(marcaGeneral.getDescripcion());
        }

        List<Marca> marcas = marcaService.findByMarcaGeneralId(id);

        return new MarcaGeneralResponse(marcaGeneralRepository.save(marcaGeneralToUpdate), marcas);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "marcasGeneral", allEntries = true)})
    public void delete(Long id) {
        MarcasGeneral marcaGeneralToDelete = marcaGeneralRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("El ID %d no fue encontrado", id)));
        marcaGeneralRepository.delete(marcaGeneralToDelete);
    }
}
