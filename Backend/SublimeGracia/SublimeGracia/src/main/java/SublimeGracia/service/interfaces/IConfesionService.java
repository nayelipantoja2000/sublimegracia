package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Confesion.ConfesionRequest;
import SublimeGracia.dto.Confesion.ConfesionResponse;
import SublimeGracia.entity.Creencia;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface IConfesionService {

    @Cacheable("confesion")
    List<ConfesionResponse> findAll();

    @Cacheable("confesion")
    ConfesionResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "confesion", allEntries = true)})
    ConfesionResponse save(ConfesionRequest confesion);

    @Caching(evict = {@CacheEvict(value = "confesion", allEntries = true)})
    ConfesionResponse update(Long id, ConfesionRequest confesion);

    @Caching(evict = {@CacheEvict(value = "confesion", allEntries = true)})
    void delete(Long id);
}
