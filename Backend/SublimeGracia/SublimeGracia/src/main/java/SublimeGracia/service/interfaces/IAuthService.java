package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Auth.AuthRequest;
import SublimeGracia.dto.Auth.AuthResponse;
import SublimeGracia.dto.Auth.EmailRequest;
import SublimeGracia.dto.Auth.ValidateMessageResponse;

import javax.servlet.http.HttpServletResponse;

public interface IAuthService {
    AuthResponse login(AuthRequest authRequest);

    ValidateMessageResponse validateUser(String code, HttpServletResponse response);

    ValidateMessageResponse resendEmail(EmailRequest email);

    ValidateMessageResponse logout(String token);


}
