package SublimeGracia.service;

import SublimeGracia.service.interfaces.ICreenciaService;
import SublimeGracia.dto.Creencia.CreenciaRequest;
import SublimeGracia.dto.Creencia.CreenciaResponse;
import SublimeGracia.entity.Confesion;
import SublimeGracia.entity.Creencia;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.IConfesionRepository;
import SublimeGracia.repository.ICreenciaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CreenciaService implements ICreenciaService {

    private final IConfesionRepository confesionRepository;
    private final ICreenciaRepository creenciaRepository;

    @Override
    @Cacheable("creencia")
    public List<CreenciaResponse> findAll() {
        return creenciaRepository.findAll().stream()
                .map(creencia -> new CreenciaResponse(creencia)).toList();
    }

    @Override
    @Cacheable("creencia")
    public CreenciaResponse findById(Long id) {
        return creenciaRepository.findById(id)
                .map(creencia -> new CreenciaResponse(creencia))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "creencia", allEntries = true)})
    public CreenciaResponse save(CreenciaRequest creencia) {
        creenciaRepository.findByTitulo(creencia.getTitulo()).ifPresent(b -> {
            throw new BadRequestException("Este título ya ha sido cargado " + creencia.getTitulo());
        });
        creenciaRepository.findByImagen(creencia.getImagen()).ifPresent(b -> {
            throw new BadRequestException("Esta imagen ya ha sido cargada " + creencia.getImagen());
        });

        Creencia creenciaToSave = new Creencia();
        creenciaToSave.setTitulo(creencia.getTitulo());
        creenciaToSave.setDescripcion(creencia.getDescripcion());
        creenciaToSave.setImagen(creencia.getImagen());

        Long confesionId = creencia.getConfesionId();
        Confesion confesion = confesionRepository.findById(confesionId).orElse(null);

        if (confesion != null) {
            creenciaToSave.setConfesion(confesion);
            return new CreenciaResponse(creenciaRepository.save(creenciaToSave));
        } else {
            return null;
        }

    }

    @Override
    @Caching(evict = {@CacheEvict(value = "creencia", allEntries = true)})
    public CreenciaResponse update(Long id, CreenciaRequest creencia) {

        Creencia creenciaToUpdate = creenciaRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (creencia.getTitulo() != null) {
            creenciaToUpdate.setTitulo(creencia.getTitulo());
        }
        if (creencia.getImagen() != null) {
            creenciaToUpdate.setImagen(creencia.getImagen());
        }
        if (creencia.getDescripcion() != null) {
            creenciaToUpdate.setDescripcion(creencia.getDescripcion());
        }


        Long confesionId = creencia.getConfesionId();
        if (confesionId != null) {
            Confesion confesion = confesionRepository.findById(confesionId)
                    .orElseThrow(() -> new ResourceNotFoundException(String.format("El ID %d no fue encontrado", confesionId)));
            creenciaToUpdate.setConfesion(confesion);
        }

        return new CreenciaResponse(creenciaRepository.save(creenciaToUpdate));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "creencia", allEntries = true)})
    public void delete(Long id) {
        Creencia creenciaToDelete = creenciaRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("El ID %d no fue encontrado", id)));
        creenciaRepository.delete(creenciaToDelete);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "creencia", allEntries = true)})
    public List<Creencia> findByConfesionId(Long creenciaId) {
        return creenciaRepository.findByConfesionId(creenciaId);
    }
}

