package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Marca.MarcaRequest;
import SublimeGracia.dto.Marca.MarcaResponse;
import SublimeGracia.entity.Marca;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface IMarcaService {
    @Cacheable("marca")
    List<MarcaResponse> findAll();

    @Cacheable("marca")
    MarcaResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "marca", allEntries = true)})
    MarcaResponse save(MarcaRequest marca);

    @Caching(evict = {@CacheEvict(value = "marca", allEntries = true)})
    MarcaResponse update(Long id, MarcaRequest marca);

    @Caching(evict = {@CacheEvict(value = "marca", allEntries = true)})
    void delete(Long id);

    List<Marca> findByMarcaGeneralId(Long marcaGeneralId);
}
