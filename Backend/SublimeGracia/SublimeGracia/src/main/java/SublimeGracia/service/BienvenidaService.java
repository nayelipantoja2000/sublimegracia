package SublimeGracia.service;

import SublimeGracia.dto.Bienvenida.BienvenidaRequest;
import SublimeGracia.dto.Bienvenida.BienvenidaResponse;
import SublimeGracia.entity.Bienvenida;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.IBienvenidaRepository;
import SublimeGracia.service.interfaces.IBienvenidaService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BienvenidaService implements IBienvenidaService {
    private final IBienvenidaRepository bienvenidaRepository;

    @Override
    @Cacheable("bienvenida")
    public List<BienvenidaResponse> findAll() {
        return bienvenidaRepository.findAll().stream()
                .map(bienvenida -> new BienvenidaResponse(bienvenida)).toList();
    }

    @Override
    @Cacheable("bienvenida")
    public BienvenidaResponse findById(Long id) {
        return bienvenidaRepository.findById(id)
                .map(bienvenida -> new BienvenidaResponse(bienvenida))
                        .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "bienvenida", allEntries = true)})
    public BienvenidaResponse save(BienvenidaRequest bienvenida) {

        bienvenidaRepository.findByImagen(bienvenida.getImagen()).ifPresent(b -> {
            throw new BadRequestException("Esta imagen ya ha sido cargada " + bienvenida.getImagen());
        });
        Bienvenida bienvenidaToSave = new Bienvenida();
        bienvenidaToSave.setDescripcion(bienvenida.getDescripcion());
        bienvenidaToSave.setImagen((bienvenida.getImagen()));
        return new BienvenidaResponse(bienvenidaRepository.save(bienvenidaToSave));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "bienvenida", allEntries = true)})
    public BienvenidaResponse update(Long id, BienvenidaRequest bienvenida) {

        Bienvenida bienvenidaToUpdate = bienvenidaRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (bienvenida.getDescripcion() != null) {
            bienvenidaToUpdate.setDescripcion(bienvenida.getDescripcion());
        }
        if (bienvenida.getImagen() != null) {
            bienvenidaToUpdate.setImagen(bienvenida.getImagen());
        }

        return new BienvenidaResponse(bienvenidaRepository.save(bienvenidaToUpdate));
    }
}
