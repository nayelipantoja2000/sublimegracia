package SublimeGracia.service;

import SublimeGracia.dto.Servicio.ServicioRequest;
import SublimeGracia.dto.Servicio.ServicioResponse;
import SublimeGracia.entity.Reunion;
import SublimeGracia.entity.Servicio;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.IReunionRepository;
import SublimeGracia.repository.IServicioRepository;
import SublimeGracia.service.interfaces.IServicioService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ServicioService implements IServicioService {

    private final IServicioRepository servicioRepository;
    private final IReunionRepository reunionRepository;

    @Override
    @Cacheable("servicio")
    public List<ServicioResponse> findAll() {
        return servicioRepository.findAll().stream()
                .map(servicio -> new ServicioResponse(servicio)).toList();
    }

    @Override
    @Cacheable("servicio")
    public ServicioResponse findById(Long id) {
        return servicioRepository.findById(id)
                .map(servicio -> new ServicioResponse(servicio))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "servicio", allEntries = true)})
    public ServicioResponse save(ServicioRequest servicio) {

        Servicio servicioToSave = new Servicio();
        servicioToSave.setNombre(servicio.getNombre());
        servicioToSave.setHora(servicio.getHora());

        Long reunionId = servicio.getReunionId();
        Reunion reunion = reunionRepository.findById(reunionId).orElse(null);

        if (reunion != null) {
            servicioToSave.setReunion(reunion);
            return new ServicioResponse(servicioRepository.save(servicioToSave));
        } else {
            return null;
        }

    }

    @Override
    @Caching(evict = {@CacheEvict(value = "servicio", allEntries = true)})
    public ServicioResponse update(Long id, ServicioRequest servicio) {

        Servicio servicioToUpdate = servicioRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (servicio.getNombre() != null) {
            servicioToUpdate.setNombre(servicio.getNombre());
        }
        if (servicio.getHora() != null) {
            servicioToUpdate.setHora(servicio.getHora());
        }


        Long reunionId = servicio.getReunionId();
        if (reunionId != null) {
            Reunion reunion = reunionRepository.findById(reunionId)
                    .orElseThrow(() -> new ResourceNotFoundException(String.format("La reunión con el ID %d no fue encontrada", reunionId)));
            servicioToUpdate.setReunion(reunion);
        }

        return new ServicioResponse(servicioRepository.save(servicioToUpdate));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "servicio", allEntries = true)})
    public void delete(Long id) {
        Servicio servicioToDelete = servicioRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("El servicio con el ID %d no fue encontrado", id)));
        servicioRepository.delete(servicioToDelete);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "servicio", allEntries = true)})
    public List<Servicio> findByReunionId(Long reunionId) {
        return servicioRepository.findByReunionId(reunionId);
    }
}
