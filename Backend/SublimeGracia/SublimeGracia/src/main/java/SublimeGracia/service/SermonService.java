package SublimeGracia.service;

import SublimeGracia.dto.Sermon.SermonRequest;
import SublimeGracia.dto.Sermon.SermonResponse;
import SublimeGracia.entity.Sermon;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.ISermonRepository;
import SublimeGracia.service.interfaces.ISermonService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SermonService implements ISermonService {
    private final ISermonRepository sermonRepository;

    @Override
    @Cacheable("sermon")
    public List<SermonResponse> findAll() {
        return sermonRepository.findAll().stream()
                .map(sermon -> new SermonResponse(sermon)).toList();
    }

    @Override
    @Cacheable("sermon")
    public SermonResponse findById(Long id) {
        return sermonRepository.findById(id)
                .map(sermon -> new SermonResponse(sermon))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "sermon", allEntries = true)})
    public SermonResponse save(SermonRequest sermon) {

        sermonRepository.findByUrlVideo(sermon.getUrlVideo()).ifPresent(b -> {
            throw new BadRequestException("Este video ya ha sido cargado " + sermon.getUrlVideo());
        });
        Sermon sermonToSave = new Sermon();
        sermonToSave.setDescripcion(sermon.getDescripcion());
        sermonToSave.setUrlVideo(sermon.getUrlVideo());
        sermonToSave.setFrase(sermon.getFrase());
        sermonToSave.setLinkYoutube(sermon.getLinkYoutube());
        return new SermonResponse(sermonRepository.save(sermonToSave));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "sermon", allEntries = true)})
    public SermonResponse update(Long id, SermonRequest sermon) {

        Sermon sermonToUpdate = sermonRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (sermon.getDescripcion() != null) {
            sermonToUpdate.setDescripcion(sermon.getDescripcion());
        }
        if (sermon.getUrlVideo() != null) {
            sermonToUpdate.setUrlVideo(sermon.getUrlVideo());
        }
        if (sermon.getFrase() != null) {
            sermonToUpdate.setFrase(sermon.getFrase());
        }
        if(sermon.getLinkYoutube() != null) {
            sermonToUpdate.setLinkYoutube(sermon.getLinkYoutube());
        }

        return new SermonResponse(sermonRepository.save(sermonToUpdate));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "sermon", allEntries = true)})
    public void delete(Long id) {
        Sermon sermonToDelete = sermonRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("El sermon con el ID %d no fue encontrado", id)));
        sermonRepository.delete(sermonToDelete);
    }

}
