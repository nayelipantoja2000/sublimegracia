package SublimeGracia.service;

import SublimeGracia.dto.Pastor.PastorRequest;
import SublimeGracia.dto.Pastor.PastorResponse;
import SublimeGracia.entity.Pastor;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.IPastorRepository;
import SublimeGracia.service.interfaces.IPastorService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PastorService implements IPastorService {

    private final IPastorRepository pastorRepository;

    @Override
    @Cacheable("pastor")
    public List<PastorResponse> findAll() {
        return pastorRepository.findAll().stream()
                .map(pastor -> new PastorResponse(pastor)).toList();
    }

    @Override
    @Cacheable("pastor")
    public PastorResponse findById(Long id) {
        return pastorRepository.findById(id)
                .map(pastor -> new PastorResponse(pastor))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "pastor", allEntries = true)})
    public PastorResponse save(PastorRequest pastor) {

        Pastor pastorToSave = new Pastor();
        pastorToSave.setNombre(pastor.getNombre());
        pastorToSave.setImagen((pastor.getImagen()));
        return new PastorResponse(pastorRepository.save(pastorToSave));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "pastor", allEntries = true)})
    public PastorResponse update(Long id, PastorRequest pastor) {

        Pastor pastorToUpdate = pastorRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (pastor.getNombre() != null) {
            pastorToUpdate.setNombre(pastor.getNombre());
        }
        if (pastor.getImagen() != null) {
            pastorToUpdate.setImagen(pastor.getImagen());
        }

        return new PastorResponse(pastorRepository.save(pastorToUpdate));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "pastor", allEntries = true)})
    public void delete(Long id) {
        Pastor pastorToDelete = pastorRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("La sola con el ID %d no fue encontrado", id)));
        pastorRepository.delete(pastorToDelete);
    }

}
