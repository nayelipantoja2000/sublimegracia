package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Bienvenida.BienvenidaRequest;
import SublimeGracia.dto.Bienvenida.BienvenidaResponse;
import java.util.List;

public interface IBienvenidaService {
    List<BienvenidaResponse> findAll();

    BienvenidaResponse findById(Long id);

    BienvenidaResponse save(BienvenidaRequest bienvenida);

    BienvenidaResponse update(Long id, BienvenidaRequest bienvenida);

}
