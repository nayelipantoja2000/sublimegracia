package SublimeGracia.service;

import SublimeGracia.service.interfaces.IConfesionService;
import SublimeGracia.dto.Confesion.ConfesionRequest;
import SublimeGracia.dto.Confesion.ConfesionResponse;
import SublimeGracia.entity.Confesion;
import SublimeGracia.entity.Creencia;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.IConfesionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;


@Service
@RequiredArgsConstructor
public class ConfesionService implements IConfesionService {

    private final IConfesionRepository confesionRepository;
    private final CreenciaService creenciaService;

    @Override
    @Cacheable("confesion")
    public List<ConfesionResponse> findAll() {
        return confesionRepository.findAll().stream()
                .map(confesion -> {
                    List<Creencia> creencia = creenciaService.findByConfesionId(confesion.getId());
                    return new ConfesionResponse(confesion, creencia);
                })
                .toList();
    }

    @Override
    @Cacheable("confesion")
    public ConfesionResponse findById(Long id) {
        List<Creencia> creencia = creenciaService.findByConfesionId(id);
        return confesionRepository.findById(id)
                .map(confesion -> new ConfesionResponse(confesion, creencia))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "confesion", allEntries = true)})
    public ConfesionResponse save(ConfesionRequest confesion) {

        Confesion confesionToSave = new Confesion();
        confesionToSave.setEncabezado(confesion.getEncabezado());
        confesionToSave.setUrlConfesion(confesion.getUrlConfesion());
        confesionToSave.setDescripcion(confesion.getDescripcion());

        List<Creencia> creencia = Collections.emptyList();
        return new ConfesionResponse(confesionRepository.save(confesionToSave), creencia);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "confesion", allEntries = true)})
    public ConfesionResponse update(Long id, ConfesionRequest confesion) {

        Confesion confesionToUpdate = confesionRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (confesion.getEncabezado() != null) {
            confesionToUpdate.setEncabezado(confesion.getEncabezado());
        }
        if (confesion.getUrlConfesion() != null) {
            confesionToUpdate.setUrlConfesion(confesion.getUrlConfesion());
        }
        if (confesion.getDescripcion() != null) {
            confesionToUpdate.setDescripcion(confesion.getDescripcion());
        }

        List<Creencia> creencia = creenciaService.findByConfesionId(id);

        return new ConfesionResponse(confesionRepository.save(confesionToUpdate), creencia);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "confesion", allEntries = true)})
    public void delete(Long id) {
        Confesion confesionToDelete = confesionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("El ID %d no fue encontrado", id)));
        confesionRepository.delete(confesionToDelete);
    }

}
