package SublimeGracia.service.interfaces;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

public interface IAWSService {
    String uploadFile(MultipartFile file);

    List<String> uploadMultipleFiles(List<MultipartFile> files);

    void deleteFile(List<String> urlList);

    String updateFile(MultipartFile newFile, String existingFileUrl);



}
