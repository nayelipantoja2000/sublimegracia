package SublimeGracia.service;

import SublimeGracia.dto.Nosotros.NosotrosRequest;
import SublimeGracia.dto.Nosotros.NosotrosResponse;
import SublimeGracia.entity.Nosotros;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.INosotrosRepository;
import SublimeGracia.service.interfaces.INosotrosService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class NosotrosService implements INosotrosService {

    private final INosotrosRepository nosotrosRepository;

    @Override
    @Cacheable("nosotros")
    public List<NosotrosResponse> findAll() {
        return nosotrosRepository.findAll().stream()
                .map(nosotros -> new NosotrosResponse(nosotros)).toList();
    }

    @Override
    @Cacheable("nosotros")
    public NosotrosResponse findById(Long id) {
        return nosotrosRepository.findById(id)
                .map(nosotros -> new NosotrosResponse(nosotros))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "nosotros", allEntries = true)})
    public NosotrosResponse save(NosotrosRequest nosotros) {

        nosotrosRepository.findByImagen(nosotros.getImagen()).ifPresent(b -> {
            throw new BadRequestException("Esta imagen ya ha sido cargada " + nosotros.getImagen());
        });
        Nosotros nosotrosToSave = new Nosotros();
        nosotrosToSave.setDescripcion(nosotros.getDescripcion());
        nosotrosToSave.setImagen((nosotros.getImagen()));
        return new NosotrosResponse(nosotrosRepository.save(nosotrosToSave));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "nosotros", allEntries = true)})
    public NosotrosResponse update(Long id, NosotrosRequest nosotros) {

        Nosotros nosotrosToUpdate = nosotrosRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (nosotros.getDescripcion() != null) {
            nosotrosToUpdate.setDescripcion(nosotros.getDescripcion());
        }
        if (nosotros.getImagen() != null) {
            nosotrosToUpdate.setImagen(nosotros.getImagen());
        }

        return new NosotrosResponse(nosotrosRepository.save(nosotrosToUpdate));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "nosotros", allEntries = true)})
    public void delete(Long id) {
        Nosotros nosotrosToDelete = nosotrosRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("El ID %d no fue encontrado", id)));
        nosotrosRepository.delete(nosotrosToDelete);
    }
}
