package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Sermon.SermonRequest;
import SublimeGracia.dto.Sermon.SermonResponse;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface ISermonService {
    @Cacheable("sermon")
    List<SermonResponse> findAll();

    @Cacheable("sermon")
    SermonResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "sermon", allEntries = true)})
    SermonResponse save(SermonRequest sermon);

    @Caching(evict = {@CacheEvict(value = "sermon", allEntries = true)})
    SermonResponse update(Long id, SermonRequest sermon);

    @Caching(evict = {@CacheEvict(value = "sermon", allEntries = true)})
    void delete(Long id);
}
