package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Nosotros.NosotrosRequest;
import SublimeGracia.dto.Nosotros.NosotrosResponse;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface INosotrosService {
    @Cacheable("nosotros")
    List<NosotrosResponse> findAll();

    @Cacheable("nosotros")
    NosotrosResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "nosotros", allEntries = true)})
    NosotrosResponse save(NosotrosRequest nosotros);

    @Caching(evict = {@CacheEvict(value = "nosotros", allEntries = true)})
    NosotrosResponse update(Long id, NosotrosRequest nosotros);

    @Caching(evict = {@CacheEvict(value = "nosotros", allEntries = true)})
    void delete(Long id);
}
