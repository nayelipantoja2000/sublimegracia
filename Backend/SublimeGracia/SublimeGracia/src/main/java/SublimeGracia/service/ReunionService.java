package SublimeGracia.service;

import SublimeGracia.service.interfaces.IReunionService;
import SublimeGracia.service.interfaces.IServicioService;
import SublimeGracia.dto.Reunion.ReunionRequest;
import SublimeGracia.dto.Reunion.ReunionResponse;
import SublimeGracia.entity.Reunion;
import SublimeGracia.entity.Servicio;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.IReunionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReunionService implements IReunionService {

    private final IReunionRepository reunionRepository;
    private final IServicioService servicioService;

    @Override
    @Cacheable("reunion")
    public List<ReunionResponse> findAll() {
        return reunionRepository.findAll().stream()
                .map(reunion -> {
                    List<Servicio> servicios = servicioService.findByReunionId(reunion.getId());
                    return new ReunionResponse(reunion, servicios);
                })
                .toList();
    }

    @Override
    @Cacheable("reunion")
    public ReunionResponse findById(Long id) {
        List<Servicio> servicios = servicioService.findByReunionId(id);
        return reunionRepository.findById(id)
                .map(reunion -> new ReunionResponse(reunion, servicios))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "reunion", allEntries = true)})
    public ReunionResponse save(ReunionRequest reunion) {

        Reunion reunionToSave = new Reunion();
        reunionToSave.setImagenReunion(reunion.getImagenReunion());
        reunionToSave.setDireccion(reunion.getDireccion());

        List<Servicio> servicios = Collections.emptyList();
        return new ReunionResponse(reunionRepository.save(reunionToSave), servicios);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "reunion", allEntries = true)})
    public ReunionResponse update(Long id, ReunionRequest reunion) {

        Reunion reunionToUpdate = reunionRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (reunion.getImagenReunion() != null) {
            reunionToUpdate.setImagenReunion(reunion.getImagenReunion());
        }
        if (reunion.getDireccion() != null) {
            reunionToUpdate.setDireccion(reunion.getDireccion());
        }

        List<Servicio> servicios = servicioService.findByReunionId(id);

        return new ReunionResponse(reunionRepository.save(reunionToUpdate), servicios);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "reunion", allEntries = true)})
    public void delete(Long id) {
        Reunion reunionToDelete = reunionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("La reunion con el ID %d no fue encontrado", id)));
        reunionRepository.delete(reunionToDelete);
    }
}

