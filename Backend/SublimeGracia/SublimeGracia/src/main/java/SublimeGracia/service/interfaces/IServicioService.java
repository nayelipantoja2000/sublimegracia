package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Servicio.ServicioRequest;
import SublimeGracia.dto.Servicio.ServicioResponse;
import SublimeGracia.entity.Servicio;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface IServicioService {


    @Cacheable("servicio")
    List<ServicioResponse> findAll();

    @Cacheable("servicio")
    ServicioResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "servicio", allEntries = true)})
    ServicioResponse save(ServicioRequest servicio);

    @Caching(evict = {@CacheEvict(value = "servicio", allEntries = true)})
    ServicioResponse update(Long id, ServicioRequest servicio);

    @Caching(evict = {@CacheEvict(value = "servicio", allEntries = true)})
    void delete(Long id);

    List<Servicio> findByReunionId(Long reunionId);
}
