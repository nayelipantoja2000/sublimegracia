package SublimeGracia.service;

import SublimeGracia.service.interfaces.IMarcaService;
import SublimeGracia.dto.Marca.MarcaRequest;
import SublimeGracia.dto.Marca.MarcaResponse;
import SublimeGracia.entity.Marca;
import SublimeGracia.entity.MarcasGeneral;
import SublimeGracia.exceptions.BadRequestException;
import SublimeGracia.exceptions.ResourceNotFoundException;
import SublimeGracia.repository.IMarcaGeneralRepository;
import SublimeGracia.repository.IMarcaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MarcaService implements IMarcaService {

    private final IMarcaRepository marcaRepository;
    private final IMarcaGeneralRepository marcaGeneralRepository;

    @Override
    @Cacheable("marca")
    public List<MarcaResponse> findAll() {
        return marcaRepository.findAll().stream()
                .map(marca -> new MarcaResponse(marca)).toList();
    }

    @Override
    @Cacheable("marca")
    public MarcaResponse findById(Long id) {
        return marcaRepository.findById(id)
                .map(marca -> new MarcaResponse(marca))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("el id ", id, " no fue encontrado")));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "marca", allEntries = true)})
    public MarcaResponse save(MarcaRequest marca) {
        marcaRepository.findByTitulo(marca.getTitulo()).ifPresent(b -> {
            throw new BadRequestException("Este título ya ha sido cargado " + marca.getTitulo());
        });
        marcaRepository.findByImagen(marca.getImagen()).ifPresent(b -> {
            throw new BadRequestException("Esta imagen ya ha sido cargada " + marca.getImagen());
        });

        Marca marcaToSave = new Marca();
        marcaToSave.setTitulo(marca.getTitulo());
        marcaToSave.setDescripcion(marca.getDescripcion());
        marcaToSave.setImagen(marca.getImagen());

        Long marcaGeneralId = marca.getMarcasGeneralId();
        MarcasGeneral marcasGeneral = marcaGeneralRepository.findById(marcaGeneralId).orElse(null);

        if (marcasGeneral != null) {
            marcaToSave.setMarcasGeneral(marcasGeneral);
            return new MarcaResponse(marcaRepository.save(marcaToSave));
        } else {
            return null;
        }

    }

    @Override
    @Caching(evict = {@CacheEvict(value = "marca", allEntries = true)})
    public MarcaResponse update(Long id, MarcaRequest marca) {

        Marca marcaToUpdate = marcaRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("el id ", id, " no fue encontrado")));
        if (marca.getTitulo() != null) {
            marcaToUpdate.setTitulo(marca.getTitulo());
        }
        if (marca.getImagen() != null) {
            marcaToUpdate.setImagen(marca.getImagen());
        }
        if (marca.getDescripcion() != null) {
            marcaToUpdate.setDescripcion(marca.getDescripcion());
        }


        Long marcasGeneralId = marca.getMarcasGeneralId();
        if (marcasGeneralId != null) {
            MarcasGeneral marcasGeneral = marcaGeneralRepository.findById(marcasGeneralId)
                    .orElseThrow(() -> new ResourceNotFoundException(String.format("El ID %d no fue encontrado", marcasGeneralId)));
            marcaToUpdate.setMarcasGeneral(marcasGeneral);
        }

        return new MarcaResponse(marcaRepository.save(marcaToUpdate));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "marca", allEntries = true)})
    public void delete(Long id) {
        Marca marcaToDelete = marcaRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("El ID %d no fue encontrado", id)));
        marcaRepository.delete(marcaToDelete);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "marca", allEntries = true)})
    public List<Marca> findByMarcaGeneralId(Long marcaGeneralId) {
        return marcaRepository.findByMarcasGeneralId(marcaGeneralId);

    }


}
