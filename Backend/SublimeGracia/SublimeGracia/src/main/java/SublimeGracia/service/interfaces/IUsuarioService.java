package SublimeGracia.service.interfaces;

import SublimeGracia.dto.Usuario.UsuarioRequest;
import SublimeGracia.dto.Usuario.UsuarioResponse;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

public interface IUsuarioService {
    @Cacheable("usuarios")
    List<UsuarioResponse> findAll();

    @Cacheable("usuario")
    UsuarioResponse findById(Long id);

    @Caching(evict = {@CacheEvict(value = "usuarios", allEntries = true), @CacheEvict(value = "usuario", allEntries = true)})
    UsuarioResponse save(UsuarioRequest usuario);

    @Caching(evict = {@CacheEvict(value = "usuarios", allEntries = true), @CacheEvict(value = "usuario", allEntries = true)})
    UsuarioResponse updateById(Long id, UsuarioRequest usuario);

    @Caching(evict = {@CacheEvict(value = "usuarios", allEntries = true), @CacheEvict(value = "usuario", allEntries = true)})
    void deleteById(Long id);
}
