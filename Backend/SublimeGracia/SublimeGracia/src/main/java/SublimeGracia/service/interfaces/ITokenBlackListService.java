package SublimeGracia.service.interfaces;

public interface ITokenBlackListService {
    void add(String token);

    boolean contains(String token);
}
