package SublimeGracia.repository;

import SublimeGracia.entity.Nosotros;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface INosotrosRepository extends JpaRepository<Nosotros, Long> {
    Optional<Nosotros> findByImagen(String imagen);
}
