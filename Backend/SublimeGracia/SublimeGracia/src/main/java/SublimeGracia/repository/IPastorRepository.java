package SublimeGracia.repository;

import SublimeGracia.entity.Pastor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPastorRepository extends JpaRepository<Pastor, Long> {
}
