package SublimeGracia.repository;

import SublimeGracia.entity.Bienvenida;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IBienvenidaRepository extends JpaRepository<Bienvenida, Long> {
    Optional<Bienvenida> findByImagen(String imagen);
}
