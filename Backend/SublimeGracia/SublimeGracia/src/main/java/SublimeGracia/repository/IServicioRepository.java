package SublimeGracia.repository;

import SublimeGracia.entity.Servicio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IServicioRepository extends JpaRepository<Servicio, Long> {
    List<Servicio> findByReunionId(Long reunionId);
}
