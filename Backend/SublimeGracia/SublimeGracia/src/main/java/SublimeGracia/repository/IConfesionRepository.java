package SublimeGracia.repository;

import SublimeGracia.entity.Confesion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IConfesionRepository extends JpaRepository<Confesion, Long> {
}
