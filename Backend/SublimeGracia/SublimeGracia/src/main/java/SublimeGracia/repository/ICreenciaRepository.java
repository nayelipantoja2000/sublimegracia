package SublimeGracia.repository;

import SublimeGracia.entity.Creencia;
import SublimeGracia.entity.Marca;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ICreenciaRepository extends JpaRepository<Creencia, Long> {
    List<Creencia> findByConfesionId(Long confesionId);

    Optional<Object> findByTitulo(String titulo);

    Optional<Object> findByImagen(String imagen);
}
