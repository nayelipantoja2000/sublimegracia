package SublimeGracia.repository;

import SublimeGracia.entity.Solas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolasRepository extends JpaRepository<Solas, Long> {
}
