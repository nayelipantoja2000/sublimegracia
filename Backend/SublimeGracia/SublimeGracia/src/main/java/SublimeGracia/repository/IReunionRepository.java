package SublimeGracia.repository;

import SublimeGracia.entity.Reunion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IReunionRepository extends JpaRepository<Reunion, Long> {
}
