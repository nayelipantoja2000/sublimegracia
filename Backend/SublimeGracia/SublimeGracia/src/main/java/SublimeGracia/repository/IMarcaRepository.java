package SublimeGracia.repository;

import SublimeGracia.entity.Marca;
import SublimeGracia.entity.MarcasGeneral;
import SublimeGracia.entity.Servicio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IMarcaRepository extends JpaRepository<Marca, Long> {
    Optional<Marca> findByTitulo(String titulo);
    Optional<Marca> findByImagen(String imagen);
    List<Marca> findByMarcasGeneralId(Long marcasGeneralId);
}
