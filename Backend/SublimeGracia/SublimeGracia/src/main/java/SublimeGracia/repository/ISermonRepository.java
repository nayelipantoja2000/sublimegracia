package SublimeGracia.repository;

import SublimeGracia.entity.Sermon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ISermonRepository extends JpaRepository<Sermon, Long> {
    Optional<Sermon> findByUrlVideo(String urlVideo);
}
