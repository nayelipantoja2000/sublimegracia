package SublimeGracia.repository;

import SublimeGracia.entity.MarcasGeneral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMarcaGeneralRepository extends JpaRepository<MarcasGeneral, Long> {
}
