package SublimeGracia.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@Table(name = "creencia")
public class Creencia {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String titulo;
    @Size(max = 800, message = "La descripción debe tener como máximo 700 caracteres.")
    private String descripcion;
    private String imagen;

    @ManyToOne
    @JoinColumn(name = "confesion_id")
    private Confesion confesion;
}
