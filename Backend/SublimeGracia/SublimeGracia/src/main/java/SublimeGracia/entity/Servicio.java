package SublimeGracia.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "servicio")
public class Servicio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String hora;

    @ManyToOne
    @JoinColumn(name = "reunion_id")
    private Reunion reunion;
}
