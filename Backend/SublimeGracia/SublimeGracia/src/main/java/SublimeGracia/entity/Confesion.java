package SublimeGracia.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@Table(name = "confesion")
public class Confesion {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private String encabezado;
    @Size(max = 3000, message = "La descripción debe tener como máximo 3000 caracteres." )
    private String descripcion;
    private String urlConfesion;
}
