package SublimeGracia.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@Table(name = "marca")
public class Marca {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String titulo;
    @Size(max = 1000, message = "La descripción debe tener como máximo 1000 caracteres.")
    private String descripcion;
    private String imagen;

    @ManyToOne
    @JoinColumn(name = "marcasGeneral_id")
    private MarcasGeneral marcasGeneral;
}
