package SublimeGracia.dto.Auth;

public record AuthResponse(String accesToken) {
}
