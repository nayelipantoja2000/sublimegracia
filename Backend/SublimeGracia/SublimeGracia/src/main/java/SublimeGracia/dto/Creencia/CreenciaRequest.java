package SublimeGracia.dto.Creencia;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreenciaRequest {
    private String titulo;
    private String descripcion;
    private String imagen;
    private Long confesionId;
}
