package SublimeGracia.dto.Bienvenida;

import SublimeGracia.entity.Bienvenida;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BienvenidaResponse {
    private Long id;
    private String descripcion;
    private String imagen;

    public BienvenidaResponse(Bienvenida bienvenida) {
        this.id = bienvenida.getId();
        this.descripcion = bienvenida.getDescripcion();
        this.imagen = bienvenida.getImagen();
    }

}
