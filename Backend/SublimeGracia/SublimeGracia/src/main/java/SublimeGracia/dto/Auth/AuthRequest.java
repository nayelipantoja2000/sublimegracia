package SublimeGracia.dto.Auth;

public record AuthRequest(String email, String password) {
}
