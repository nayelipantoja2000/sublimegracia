package SublimeGracia.dto.Confesion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfesionRequest {
    private String encabezado;
    private String descripcion;
    private String urlConfesion;
}
