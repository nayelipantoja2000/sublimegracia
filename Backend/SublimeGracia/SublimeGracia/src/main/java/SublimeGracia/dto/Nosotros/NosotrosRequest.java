package SublimeGracia.dto.Nosotros;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NosotrosRequest {
    private String imagen;
    private String descripcion;
}
