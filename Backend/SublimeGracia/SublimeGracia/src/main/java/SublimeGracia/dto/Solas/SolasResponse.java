package SublimeGracia.dto.Solas;

import SublimeGracia.entity.Solas;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SolasResponse {
    private Long id;
    private String titulo;
    private String descripcion;

    public SolasResponse(Solas solas){
        this.id = solas.getId();
        this.titulo = solas.getTitulo();
        this.descripcion = solas.getDescripcion();
    }
}
