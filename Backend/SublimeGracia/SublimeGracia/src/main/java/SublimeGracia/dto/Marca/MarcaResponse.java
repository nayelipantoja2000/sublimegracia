package SublimeGracia.dto.Marca;

import SublimeGracia.entity.Marca;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MarcaResponse {
    private Long id;
    private String titulo;
    private String descripcion;
    private String imagen;
    private Long marcasGeneralId;

    public MarcaResponse(Marca marca){
        this.id= marca.getId();
        this.titulo = marca.getTitulo();
        this.descripcion = marca.getDescripcion();
        this.imagen = marca.getImagen();
        this.marcasGeneralId = marca.getMarcasGeneral().getId();
    }
}
