package SublimeGracia.dto.Solas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SolasRequest {
    private String titulo;
    private String descripcion;
}
