package SublimeGracia.dto.Confesion;

import SublimeGracia.dto.Creencia.CreenciaResponse;
import SublimeGracia.entity.Confesion;
import SublimeGracia.entity.Creencia;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class ConfesionResponse {
    private Long id;
    private String encabezado;
    private String descripcion;
    private String urlConfesion;
    private List<CreenciaResponse> creencias;

    public ConfesionResponse(Confesion confesion, List<Creencia> creencia){
        this.id = confesion.getId();
        this.encabezado = confesion.getEncabezado();
        this.descripcion = confesion.getDescripcion();
        this.urlConfesion = confesion.getUrlConfesion();
        this.creencias = creencia.stream()
                .map(CreenciaResponse::new)
                .collect(Collectors.toList());

    }
}
