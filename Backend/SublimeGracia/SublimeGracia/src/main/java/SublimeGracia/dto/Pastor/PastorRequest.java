package SublimeGracia.dto.Pastor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PastorRequest {
    private Long Id;
    private String nombre;
    private String imagen;
}
