package SublimeGracia.dto.Reunion;

import SublimeGracia.dto.Servicio.ServicioResponse;
import SublimeGracia.entity.Reunion;
import SublimeGracia.entity.Servicio;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class ReunionResponse {
    private Long id;
    private String imagenReunion;
    private String direccion;
    private List<ServicioResponse> servicios;

    public ReunionResponse(Reunion reunion, List<Servicio> servicios){
        this.id = reunion.getId();
        this.imagenReunion = reunion.getImagenReunion();
        this.direccion = reunion.getDireccion();
        this.servicios = servicios.stream()
                .map(ServicioResponse::new)
                .collect(Collectors.toList());
    }
}
