package SublimeGracia.dto.Error;

public record ErrorResponse(int status, String message) {
}
