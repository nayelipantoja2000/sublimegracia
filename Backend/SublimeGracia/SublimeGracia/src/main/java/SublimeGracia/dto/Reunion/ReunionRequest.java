package SublimeGracia.dto.Reunion;

import SublimeGracia.entity.Servicio;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReunionRequest {
    private String imagenReunion;
    private String direccion;
}
