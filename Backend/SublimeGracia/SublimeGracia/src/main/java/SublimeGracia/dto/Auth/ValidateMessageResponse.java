package SublimeGracia.dto.Auth;

public record ValidateMessageResponse(String message) {
}
