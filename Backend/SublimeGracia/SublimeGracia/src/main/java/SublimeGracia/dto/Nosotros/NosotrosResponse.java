package SublimeGracia.dto.Nosotros;

import SublimeGracia.entity.Nosotros;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NosotrosResponse {
    private Long id;
    private String imagen;
    private String descripcion;

    public NosotrosResponse(Nosotros nosotros){
        this.id = nosotros.getId();
        this.imagen = nosotros.getImagen();
        this.descripcion = nosotros.getDescripcion();
    }
}
