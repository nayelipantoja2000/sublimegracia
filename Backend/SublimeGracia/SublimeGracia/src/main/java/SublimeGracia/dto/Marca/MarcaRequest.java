package SublimeGracia.dto.Marca;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarcaRequest {
    private String titulo;
    private String descripcion;
    private String imagen;

    private Long marcasGeneralId;
}
