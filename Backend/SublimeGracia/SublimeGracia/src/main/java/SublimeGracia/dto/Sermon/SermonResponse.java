package SublimeGracia.dto.Sermon;

import SublimeGracia.entity.Sermon;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SermonResponse {
    private Long id;
    private String urlVideo;
    private String linkYoutube;
    private String frase;
    private String descripcion;

    public SermonResponse(Sermon sermon){
        this.id = sermon.getId();
        this.urlVideo = sermon.getUrlVideo();;
        this.linkYoutube = sermon.getLinkYoutube();
        this.descripcion = sermon.getDescripcion();
        this.frase = sermon.getFrase();
    }
}
