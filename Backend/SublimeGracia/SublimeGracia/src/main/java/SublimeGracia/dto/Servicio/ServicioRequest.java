package SublimeGracia.dto.Servicio;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServicioRequest {
    private String nombre;
    private String hora;
    private Long reunionId;
}
