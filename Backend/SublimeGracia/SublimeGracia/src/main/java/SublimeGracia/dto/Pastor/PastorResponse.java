package SublimeGracia.dto.Pastor;

import SublimeGracia.entity.Pastor;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PastorResponse {
    private Long id;
    private String nombre;
    private String imagen;

    public PastorResponse(Pastor pastor){
        this.id = pastor.getId();
        this.nombre = pastor.getNombre();
        this.imagen = pastor.getImagen();
    }
}
