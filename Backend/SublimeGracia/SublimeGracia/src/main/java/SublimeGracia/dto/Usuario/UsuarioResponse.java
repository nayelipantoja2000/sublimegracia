package SublimeGracia.dto.Usuario;

import SublimeGracia.entity.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UsuarioResponse {
    private String nombre;
    private String apellido;
    private Integer documento;
    private String email;
    private Integer telefono;

    public UsuarioResponse(Usuario usuario) {
        this.nombre = usuario.getNombre();
        this.apellido = usuario.getApellido();
        this.documento = usuario.getDocumento();
        this.email = usuario.getEmail();
        this.telefono = usuario.getTelefono();
    }
}
