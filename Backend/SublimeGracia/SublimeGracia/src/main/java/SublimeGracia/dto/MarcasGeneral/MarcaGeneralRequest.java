package SublimeGracia.dto.MarcasGeneral;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarcaGeneralRequest {
    private String encabezado;
    private String descripcion;
    private String urlMarcas;
}
