package SublimeGracia.dto.MarcasGeneral;

import SublimeGracia.dto.Marca.MarcaResponse;
import SublimeGracia.entity.Marca;
import SublimeGracia.entity.MarcasGeneral;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class MarcaGeneralResponse {
    private Long id;
    private String encabezado;
    private String descripcion;
    private String urlMarcas;
    private List<MarcaResponse> marcas;

    public MarcaGeneralResponse(MarcasGeneral marcaGeneral, List<Marca> marca){
        this.id = marcaGeneral.getId();
        this.encabezado = marcaGeneral.getEncabezado();
        this.descripcion = marcaGeneral.getDescripcion();
        this.urlMarcas = marcaGeneral.getUrlMarcas();
        this.marcas = marca.stream()
                .map(MarcaResponse::new)
                .collect(Collectors.toList());
    }
}
