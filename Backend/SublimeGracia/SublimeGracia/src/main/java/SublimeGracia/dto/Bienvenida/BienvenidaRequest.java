package SublimeGracia.dto.Bienvenida;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BienvenidaRequest {
    private String descripcion;
    private String imagen;
}
