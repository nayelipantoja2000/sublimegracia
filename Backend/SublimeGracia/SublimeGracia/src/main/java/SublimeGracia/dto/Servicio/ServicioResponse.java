package SublimeGracia.dto.Servicio;
import SublimeGracia.entity.Servicio;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class ServicioResponse {
    private Long id;
    private String nombre;
    private String hora;
    private Long reunionId;

    public ServicioResponse(Servicio servicio){
        this.id = servicio.getId();
        this.nombre = servicio.getNombre();
        this.hora = servicio.getHora();
        this.reunionId = servicio.getReunion().getId();
    }
}
