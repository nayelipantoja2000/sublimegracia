package SublimeGracia.dto.Creencia;

import SublimeGracia.entity.Creencia;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreenciaResponse {
    private Long id;
    private String titulo;
    private String descripcion;
    private String imagen;
    private Long confesionId;

    public CreenciaResponse(Creencia creencia){
        this.id = creencia.getId();
        this.titulo = creencia.getTitulo();
        this.descripcion = creencia.getDescripcion();
        this.confesionId = creencia.getConfesion().getId();
        this.imagen = creencia.getImagen();
    }
}
