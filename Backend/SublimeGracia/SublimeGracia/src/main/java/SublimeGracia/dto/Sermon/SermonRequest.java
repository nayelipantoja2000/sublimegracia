package SublimeGracia.dto.Sermon;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SermonRequest {
    private String urlVideo;
    private String linkYoutube;
    private String frase;
    private String descripcion;
}
