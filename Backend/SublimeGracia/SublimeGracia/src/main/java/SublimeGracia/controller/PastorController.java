package SublimeGracia.controller;

import SublimeGracia.service.PastorService;
import SublimeGracia.dto.Pastor.PastorRequest;
import SublimeGracia.dto.Pastor.PastorResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pastor")
@RequiredArgsConstructor
public class PastorController {
    private final PastorService pastorService;

    @GetMapping
    public ResponseEntity<List<PastorResponse>> listarPastor() {
        return ResponseEntity.ok(pastorService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PastorResponse> buscarPastor(@PathVariable Long id) {
        return ResponseEntity.ok(pastorService.findById(id));
    }

    @PostMapping
    public ResponseEntity<PastorResponse> guardarPastor(@RequestBody PastorRequest pastor) {
        return ResponseEntity.status(HttpStatus.CREATED).body(pastorService.save(pastor));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PastorResponse> updateById(@PathVariable Long id, @RequestBody PastorRequest pastor) {
        return ResponseEntity.status(HttpStatus.OK).body(pastorService.update(id, pastor));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePastor(@PathVariable Long id) {
        pastorService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
