package SublimeGracia.controller;

import SublimeGracia.dto.Sermon.SermonRequest;
import SublimeGracia.dto.Sermon.SermonResponse;
import SublimeGracia.service.SermonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sermones")
@RequiredArgsConstructor
public class SermonController {

    private final SermonService sermonService;

    @GetMapping
    public ResponseEntity<List<SermonResponse>> listarSermon() {
        return ResponseEntity.ok(sermonService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<SermonResponse> buscarSermon(@PathVariable Long id) {
        return ResponseEntity.ok(sermonService.findById(id));
    }

    @PostMapping
    public ResponseEntity<SermonResponse> guardarSermon(@RequestBody SermonRequest sermon) {
        return ResponseEntity.status(HttpStatus.CREATED).body(sermonService.save(sermon));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SermonResponse> updateById(@PathVariable Long id, @RequestBody SermonRequest sermon) {
        return ResponseEntity.status(HttpStatus.OK).body(sermonService.update(id, sermon));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSermon(@PathVariable Long id) {
        sermonService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
