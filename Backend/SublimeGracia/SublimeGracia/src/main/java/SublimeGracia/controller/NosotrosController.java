package SublimeGracia.controller;

import SublimeGracia.dto.Nosotros.NosotrosRequest;
import SublimeGracia.dto.Nosotros.NosotrosResponse;
import SublimeGracia.service.NosotrosService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/nosotros")
@RequiredArgsConstructor
public class NosotrosController {

    private final NosotrosService nosotrosService;

    @GetMapping
    public ResponseEntity<List<NosotrosResponse>> listarNosotros() {
        return ResponseEntity.ok(nosotrosService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<NosotrosResponse> buscarNosotros(@PathVariable Long id) {
        return ResponseEntity.ok(nosotrosService.findById(id));
    }

    @PostMapping
    public ResponseEntity<NosotrosResponse> guardarNosotros(@RequestBody NosotrosRequest nosotros) {
        return ResponseEntity.status(HttpStatus.CREATED).body(nosotrosService.save(nosotros));
    }

    @PutMapping("/{id}")
    public ResponseEntity<NosotrosResponse> updateById(@PathVariable Long id, @RequestBody NosotrosRequest nosotros) {
        return ResponseEntity.status(HttpStatus.OK).body(nosotrosService.update(id, nosotros));
    }

}
