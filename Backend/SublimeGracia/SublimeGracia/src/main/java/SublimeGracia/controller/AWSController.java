package SublimeGracia.controller;

import SublimeGracia.service.AWSService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.util.List;

@RestController
@RequestMapping("/s3")
@RequiredArgsConstructor
public class AWSController {

    private final AWSService awsService;


    @PostMapping("/upload")
    public ResponseEntity<String> uploadFile(@RequestPart(value = "file") MultipartFile file) {
        return ResponseEntity.ok(awsService.uploadFile(file));
    }

    @PostMapping("/Mupload")
    public ResponseEntity<List<String>> uploadMultiplesFiles(@RequestPart(value = "file") List<MultipartFile> files) {
        return ResponseEntity.ok(awsService.uploadMultipleFiles(files));
    }

    @DeleteMapping
    public ResponseEntity<String> deleteFile(@RequestBody List<String> files) {
        awsService.deleteFile(files);
        return ResponseEntity.ok("Se eliminó el archivo");
    }

    @PutMapping("/update")
    public ResponseEntity<String> updateFile(@RequestParam("file") MultipartFile file, @RequestParam("existingFileUrl") String existingFileUrl) {
        String updatedFileUrl = awsService.updateFile(file, existingFileUrl);
        if (updatedFileUrl != null) {
            return ResponseEntity.ok("Archivo actualizado en la URL: " + updatedFileUrl);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al actualizar el archivo.");
        }
    }
}
