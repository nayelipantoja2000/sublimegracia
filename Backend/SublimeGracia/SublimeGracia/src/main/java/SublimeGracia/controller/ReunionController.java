package SublimeGracia.controller;

import SublimeGracia.dto.Reunion.ReunionRequest;
import SublimeGracia.dto.Reunion.ReunionResponse;
import SublimeGracia.service.ReunionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/reunion")
public class ReunionController {
    private final ReunionService reunionService;

    @GetMapping
    public ResponseEntity<List<ReunionResponse>> listarreunion() {
        return ResponseEntity.ok(reunionService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReunionResponse> buscarReunion(@PathVariable Long id) {
        return ResponseEntity.ok(reunionService.findById(id));
    }

    @PostMapping
    public ResponseEntity<ReunionResponse> guardarReunion(@RequestBody ReunionRequest reunion) {
        return ResponseEntity.status(HttpStatus.CREATED).body(reunionService.save(reunion));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ReunionResponse> updateById(@PathVariable Long id, @RequestBody ReunionRequest reunion) {
        return ResponseEntity.status(HttpStatus.OK).body(reunionService.update(id, reunion));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteReunion(@PathVariable Long id) {
        reunionService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
