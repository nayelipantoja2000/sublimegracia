package SublimeGracia.controller;

import SublimeGracia.dto.Creencia.CreenciaRequest;
import SublimeGracia.dto.Creencia.CreenciaResponse;
import SublimeGracia.service.CreenciaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/creencia")
@RequiredArgsConstructor
public class CreenciaController {

    private final CreenciaService creenciaService;

    @GetMapping
    public ResponseEntity<List<CreenciaResponse>> listarCreencia() {
        return ResponseEntity.ok(creenciaService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<CreenciaResponse> creenciaCreencia(@PathVariable Long id) {
        return ResponseEntity.ok(creenciaService.findById(id));
    }

    @PostMapping
    public ResponseEntity<CreenciaResponse> guardarCreencia(@RequestBody CreenciaRequest creencia) {
        return ResponseEntity.status(HttpStatus.CREATED).body(creenciaService.save(creencia));
    }

    @PutMapping("/{id}")
    public ResponseEntity<CreenciaResponse> updateById(@PathVariable Long id, @RequestBody CreenciaRequest creencia) {
        return ResponseEntity.status(HttpStatus.OK).body(creenciaService.update(id, creencia));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletecCreencia(@PathVariable Long id) {
        creenciaService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
