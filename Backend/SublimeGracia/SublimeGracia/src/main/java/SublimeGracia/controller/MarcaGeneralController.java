package SublimeGracia.controller;

import SublimeGracia.dto.MarcasGeneral.MarcaGeneralRequest;
import SublimeGracia.dto.MarcasGeneral.MarcaGeneralResponse;
import SublimeGracia.service.MarcasGeneralService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/marcasDeUnaIglesiaSaludable")
public class MarcaGeneralController {

    private final MarcasGeneralService marcasGeneralService;

    @GetMapping
    public ResponseEntity<List<MarcaGeneralResponse>> listarMarcaGeneral() {
        return ResponseEntity.ok(marcasGeneralService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<MarcaGeneralResponse> buscarMarcaGeneral(@PathVariable Long id) {
        return ResponseEntity.ok(marcasGeneralService.findById(id));
    }

    @PostMapping
    public ResponseEntity<MarcaGeneralResponse> guardarMarcaGeneral(@RequestBody MarcaGeneralRequest marcaGeneral) {
        return ResponseEntity.status(HttpStatus.CREATED).body(marcasGeneralService.save(marcaGeneral));
    }

    @PutMapping("/{id}")
    public ResponseEntity<MarcaGeneralResponse> updateById(@PathVariable Long id, @RequestBody MarcaGeneralRequest marcaGeneral) {
        return ResponseEntity.status(HttpStatus.OK).body(marcasGeneralService.update(id, marcaGeneral));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMarcaGeneral(@PathVariable Long id) {
        marcasGeneralService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
