package SublimeGracia.controller;

import SublimeGracia.dto.Confesion.ConfesionRequest;
import SublimeGracia.dto.Confesion.ConfesionResponse;
import SublimeGracia.service.ConfesionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/confesionDeFe")
public class ConfesionController {

    private final ConfesionService confesionService;

    @GetMapping
    public ResponseEntity<List<ConfesionResponse>> listarConfesion() {
        return ResponseEntity.ok(confesionService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ConfesionResponse> buscarConfesion(@PathVariable Long id) {
        return ResponseEntity.ok(confesionService.findById(id));
    }

    @PostMapping
    public ResponseEntity<ConfesionResponse> guardarConfesion(@RequestBody ConfesionRequest confesion) {
        return ResponseEntity.status(HttpStatus.CREATED).body(confesionService.save(confesion));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ConfesionResponse> updateById(@PathVariable Long id, @RequestBody ConfesionRequest confesion) {
        return ResponseEntity.status(HttpStatus.OK).body(confesionService.update(id, confesion));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteConfesion(@PathVariable Long id) {
        confesionService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
