package SublimeGracia.controller;

import SublimeGracia.dto.Auth.ValidateMessageResponse;
import SublimeGracia.dto.Usuario.UsuarioRequest;
import SublimeGracia.dto.Usuario.UsuarioResponse;
import SublimeGracia.service.UsuarioService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
@RequiredArgsConstructor
public class UsuarioController {

    private final UsuarioService usuarioService;

    @GetMapping
    public ResponseEntity<List<UsuarioResponse>> listarUsuarios() {
        return ResponseEntity.ok(usuarioService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UsuarioResponse> buscarCliente(@PathVariable Long id) {
        return ResponseEntity.ok(usuarioService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> eliminarCliente(@PathVariable Long id) {
        usuarioService.deleteById(id);
        return ResponseEntity.ok("Se elimino el cliente de id: " + id);
    }
}
