package SublimeGracia.controller;

import SublimeGracia.dto.Servicio.ServicioRequest;
import SublimeGracia.dto.Servicio.ServicioResponse;
import SublimeGracia.service.ServicioService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/servicio")
public class ServicioController {
    private final ServicioService servicioService;

    @GetMapping
    public ResponseEntity<List<ServicioResponse>> listarServicio() {
        return ResponseEntity.ok(servicioService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ServicioResponse> buscarServicio(@PathVariable Long id) {
        return ResponseEntity.ok(servicioService.findById(id));
    }

    @PostMapping
    public ResponseEntity<ServicioResponse> guardarServicio(@RequestBody ServicioRequest servicio) {
        return ResponseEntity.status(HttpStatus.CREATED).body(servicioService.save(servicio));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ServicioResponse> updateById(@PathVariable Long id, @RequestBody ServicioRequest servicio) {
        return ResponseEntity.status(HttpStatus.OK).body(servicioService.update(id, servicio));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteServicio(@PathVariable Long id) {
        servicioService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
