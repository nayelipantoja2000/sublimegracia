package SublimeGracia.controller;

import SublimeGracia.dto.Bienvenida.BienvenidaRequest;
import SublimeGracia.dto.Bienvenida.BienvenidaResponse;
import SublimeGracia.service.BienvenidaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bienvenida")
@RequiredArgsConstructor
public class BienvenidaController {

    private final BienvenidaService bienvenidaService;

    @GetMapping
    public ResponseEntity<List<BienvenidaResponse>> listarBienvenida() {
        return ResponseEntity.ok(bienvenidaService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<BienvenidaResponse> buscarBienvenida(@PathVariable Long id) {
        return ResponseEntity.ok(bienvenidaService.findById(id));
    }

    @PostMapping
    public ResponseEntity<BienvenidaResponse> guardarBienvenida(@RequestBody BienvenidaRequest bienvenida) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bienvenidaService.save(bienvenida));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BienvenidaResponse> updateById(@PathVariable Long id, @RequestBody BienvenidaRequest bienvenida) {
        return ResponseEntity.status(HttpStatus.OK).body(bienvenidaService.update(id, bienvenida));
    }
}
