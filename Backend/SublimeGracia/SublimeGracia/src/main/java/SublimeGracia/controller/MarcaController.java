package SublimeGracia.controller;

import SublimeGracia.dto.Marca.MarcaRequest;
import SublimeGracia.dto.Marca.MarcaResponse;
import SublimeGracia.service.MarcaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/marca")
public class MarcaController {
    private final MarcaService marcaService;

    @GetMapping
    public ResponseEntity<List<MarcaResponse>> listarMarca() {
        return ResponseEntity.ok(marcaService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<MarcaResponse> buscarMarca(@PathVariable Long id) {
        return ResponseEntity.ok(marcaService.findById(id));
    }

    @PostMapping
    public ResponseEntity<MarcaResponse> guardarMarca(@RequestBody MarcaRequest marca) {
        return ResponseEntity.status(HttpStatus.CREATED).body(marcaService.save(marca));
    }

    @PutMapping("/{id}")
    public ResponseEntity<MarcaResponse> updateById(@PathVariable Long id, @RequestBody MarcaRequest marca) {
        return ResponseEntity.status(HttpStatus.OK).body(marcaService.update(id, marca));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMarca(@PathVariable Long id) {
        marcaService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
