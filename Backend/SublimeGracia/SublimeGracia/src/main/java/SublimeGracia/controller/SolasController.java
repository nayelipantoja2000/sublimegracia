package SublimeGracia.controller;

import SublimeGracia.dto.Solas.SolasRequest;
import SublimeGracia.dto.Solas.SolasResponse;
import SublimeGracia.service.SolasService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/solas")
public class SolasController {
    private final SolasService solasService;

    @GetMapping
    public ResponseEntity<List<SolasResponse>> listarSolas() {
        return ResponseEntity.ok(solasService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<SolasResponse> buscarSolas(@PathVariable Long id) {
        return ResponseEntity.ok(solasService.findById(id));
    }

    @PostMapping
    public ResponseEntity<SolasResponse> guardarSolas(@RequestBody SolasRequest solas) {
        return ResponseEntity.status(HttpStatus.CREATED).body(solasService.save(solas));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SolasResponse> updateById(@PathVariable Long id, @RequestBody SolasRequest solas) {
        return ResponseEntity.status(HttpStatus.OK).body(solasService.update(id, solas));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSolas(@PathVariable Long id) {
        solasService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
